#include <iostream>
#include <stdexcept>

// Простая реализация класса Stack
template<typename T>
class Stack
{
public:
    typedef T value_type;

public:
    Stack() : reservedSize(10), pStorage(new T[reservedSize]), currentSize(0)
    {
    }

    Stack(size_t reserve) : reservedSize(reserve), currentSize(0), pStorage(nullptr)
    {
        if (reservedSize <= 0)                                  // Проверка на некорректное значение резерва
            throw std::invalid_argument("Wrong reserve value"); // Кидаем исключение

        pStorage = new T[reservedSize];
    }

    Stack(Stack &&rhs) : reservedSize(rhs.reservedSize), pStorage(rhs.pStorage), currentSize(rhs.currentSize)
    {
        rhs.pStorage = nullptr;
    }

    ~Stack()
    {
        if(pStorage)
            delete[] pStorage;
    }

    Stack& operator=(Stack&& rhs)
    {
        currentSize = rhs.currentSize;
        reservedSize = rhs.reservedSize;
        pStorage = rhs.pStorage;

        rhs.pStorage = nullptr;

        return *this;
    }

    Stack(const Stack&) = delete;
    Stack& operator= (const Stack&) = delete;

public:
    // Добавление нового элемента в стэк
    void push(const T& val)
	{
		if (currentSize == reservedSize) // Нет места для нового элемента, увеличиваем
		{
            reservedSize <<= 1;
			auto pTempStorage = new T[reservedSize];
            
            if (!pTempStorage)                                  // Не удалось выделить память
                throw std::runtime_error("Not enough memory");  // Кидаем исключение

			memcpy_s(pTempStorage, reservedSize * sizeof(T), pStorage, currentSize * sizeof(T));
			delete[] pStorage;

			pStorage = pTempStorage;
		}

		pStorage[currentSize++] = val;
	}

    // Изъятие последнего элемента из стэка
    T pop()
    {
        if (!currentSize)                               // Стек пустой
            throw std::runtime_error("Stack is empty"); // Кидаем исключение

        return pStorage[--currentSize];
    }

    // Количество элементов в стэке
    size_t size() const
    {
        return currentSize;
    }

private:
    size_t currentSize;
    size_t reservedSize;

    T* pStorage;
};

int main()
{
    Stack<int> st;

    for (auto i = 0; i < 21; ++i)
        st.push(i);

    for (auto i = st.size(); 0 < i; --i)
        std::cout << st.pop() << std::endl;
}